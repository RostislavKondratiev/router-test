import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const ROUTES: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'visits'},
  {path: 'visits', loadChildren: './visits/visits.module#VisitsModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
