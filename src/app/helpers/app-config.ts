export class AppConfig {
  public static base = 'http://localhost:3000';
  public static visits = `${AppConfig.base}/visits`;
}
