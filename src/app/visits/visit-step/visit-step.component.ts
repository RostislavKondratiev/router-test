import { Component, OnDestroy, OnInit } from '@angular/core';
import { VisitPageService } from '../shared/visit-page.service';
import { Subscription } from 'rxjs/Subscription';
import { VisitsService } from '../shared/visits.service';

@Component({
  selector: 'app-visit-step',
  templateUrl: './visit-step.component.html',
  styleUrls: ['./visit-step.component.css']
})
export class VisitStepComponent implements OnInit, OnDestroy {

  public visit;
  private _currentVisit$: Subscription;

  constructor(private visitService: VisitPageService, private vis: VisitsService) {
  }

  public ngOnInit() {
    this._currentVisit$ = this.visitService.currentItem.subscribe((res) => {
      this.visit = res;
    });
  }

  public ngOnDestroy() {
    this._currentVisit$.unsubscribe();
  }

  public nextStep() {
    if (this.visit.step < 4) {
      this.vis.updateVisit(this.visit.id, {...this.visit, step: ++this.visit.step}).subscribe((v: any) => {
        console.log(v);
        this.visitService.addCurrentStep(v.step);
      });
    }
  }

  public reset() {
    this.vis.updateVisit(this.visit.id, {...this.visit, step: 1}).subscribe((v: any) => {
      this.visitService.addCurrentStep(v.step);
    });
  }

}
