import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VisitsComponent} from './visits.component';
import {VisitItemComponent} from './visit-item/visit-item.component';
import {VisitsResolver} from './shared/visits.resolver';
import {VisitItemResolver} from './shared/visit-item.resolver';
import {VisitStepComponent} from './visit-step/visit-step.component';
import {StepsGuard} from './guards/steps.guard';

const ROUTES: Routes = [
  {path: '', component: VisitsComponent, resolve: {visits: VisitsResolver},
    children: [
      {path: ':id', component: VisitItemComponent, resolve: {visit: VisitItemResolver},
        // canActivateChild: [StepsGuard],
        // children: [
        //   {path: 'delivery', component: VisitStepComponent},
        //   {path: 'admission', component: VisitStepComponent},
        //   {path: 'inwork', component: VisitStepComponent},
        //   {path: 'extradition', component: VisitStepComponent}
        // ]
      }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class VisitsRoutingModule {}
