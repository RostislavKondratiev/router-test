import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { VisitPageService } from './shared/visit-page.service';

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.scss'],
  // providers: [VisitPageService]
})
export class VisitsComponent implements OnInit, OnDestroy {

  public visits = [];
  public displayList = true;
  public displayText = true;
  private routeSub: Subscription;
  private visitPageState$: Subscription;

  constructor(private route: ActivatedRoute,
              private visitPageService: VisitPageService,
              private cd: ChangeDetectorRef) {
  }

  public ngOnInit() {
    this.routeSub = this.route.data.subscribe((res) => {
      this.visits = res.visits;
    });

    this.visitPageState$ = this.visitPageService.currentStep.subscribe((v) => {
      if (v) {
        this.displayList = v !== 3;
        this.cd.detectChanges();
      }
      // console.log(v.step !== 3);
    });
  }

  public ngOnDestroy() {
    this.visitPageState$.unsubscribe();
  }
}
