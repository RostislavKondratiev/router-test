import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location, PlatformLocation } from '@angular/common';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { VisitPageService } from '../shared/visit-page.service';
import { stepMap } from '../shared/steps-map';

import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'app-visit-item',
  templateUrl: './visit-item.component.html',
  styleUrls: ['./visit-item.component.css']
})
export class VisitItemComponent implements OnInit, OnDestroy {

  public visit = null;

  private routeSub$: Subscription;
  private routerEvent$: Subscription;
  private step$: Subscription;


  constructor(private router: Router,
              private route: ActivatedRoute,
              private visitService: VisitPageService,
              private location: Location) {
  }

  public ngOnInit() {
    console.log(this.router);
    this.routeSub$ = this.route.data.subscribe((res) => {
      this.visit = res.visit;
      this.visitService.addCurrentItem(this.visit);
      this.visitService.addCurrentStep(this.visit.step);
      // this.navigateToStep();
    });
    this.routerEvent$ = this.router.events.debounceTime(50).subscribe((e) => {
      console.log(e);
      if (e instanceof NavigationEnd) {
        if (e.url.split('/').length === 3) {
          // this.navigateToStep();
        }
      }
    });
    this.step$ = this.visitService.currentStep.subscribe((v) => {
      console.log('current step', v);
      this.visit.step = v;
      // this.navigateToStep();
    });
  }

  public ngOnDestroy() {
    this.routerEvent$.unsubscribe();
    this.routeSub$.unsubscribe();
    this.step$.unsubscribe();
  }

  private navigateToStep() {
    const currentStep = this.visitService.stepMap.find((i) => i.step === this.visit.step);
    const route = `/visits/${this.route.snapshot.params.id}/${currentStep.url}`;
    this.router.navigate([route]);
  }
}
