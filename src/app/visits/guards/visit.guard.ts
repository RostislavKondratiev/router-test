import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class VisitGuard implements CanActivate {
  public canActivate() {
    return true;
  }
}
