import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {VisitPageService} from '../shared/visit-page.service';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/find';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';

@Injectable()
export class StepsGuard implements CanActivateChild {
  constructor(private service: VisitPageService, private router: Router) {
  }

  public canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean  {
    const steps = this.service.stepMap;
    return this.service.currentStep
      .mergeMap((val) => {
        if (val) {
          return Observable.from(steps).find((i) => i.step === val);
        } else {
          this.router.navigate([state.url.split('/').slice(0, -1).join('/')]);
          return Observable.of(null);
        }
      })
      .map((res) => res && route.routeConfig.path === res.url);
  }
}
