import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {VisitsRoutingModule} from './visits-routing.module';
import {VisitsComponent} from './visits.component';
import {VisitItemComponent} from './visit-item/visit-item.component';
import {VisitsService} from './shared/visits.service';
import {VisitsResolver} from './shared/visits.resolver';
import {VisitItemResolver} from './shared/visit-item.resolver';
import { VisitStepComponent } from './visit-step/visit-step.component';
import {VisitPageService} from './shared/visit-page.service';
import {StepsGuard} from './guards/steps.guard';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    VisitsRoutingModule
  ],
  declarations: [
    VisitsComponent,
    VisitItemComponent,
    VisitStepComponent
  ],
  providers: [
    VisitsService,
    VisitsResolver,
    VisitPageService,
    VisitItemResolver,
    StepsGuard,
  ],
})
export class VisitsModule {}
