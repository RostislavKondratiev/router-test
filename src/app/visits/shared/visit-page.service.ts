import { Injectable } from '@angular/core';
import { stepMap } from './steps-map';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/distinctUntilChanged';

@Injectable()
export class VisitPageService {

  public stepMap = stepMap;

  private _currentStep = new BehaviorSubject(null);
  private _currentItem = new BehaviorSubject(null);

  constructor() {
  }

  public get currentStep() {
    return this._currentStep.asObservable();
  }

  public addCurrentStep(val) {
    this._currentStep.next(val);
  }

  public get currentItem() {
    return this._currentItem.asObservable();
  }

  public addCurrentItem(val) {
    this._currentItem.next(val);
  }
}
