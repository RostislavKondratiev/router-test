import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../helpers/app-config';

@Injectable()
export class VisitsService {
  constructor(private http: HttpClient) {
  }

  public getVisits() {
    return this.http.get(AppConfig.visits);
  }

  public getVisit(id) {
    return this.http.get(`${AppConfig.visits}/${id}`);
  }

  public updateVisit(id, data) {
    return this.http.put(`${AppConfig.visits}/${id}`, data);
  }
}
