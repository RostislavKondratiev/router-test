import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {VisitsService} from './visits.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class VisitsResolver implements Resolve<any> {
  constructor(private service: VisitsService) {
  }

  public resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.service.getVisits();
  }
}
