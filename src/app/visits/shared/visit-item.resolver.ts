import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {VisitsService} from './visits.service';
import {Observable} from 'rxjs/Observable';
import {VisitPageService} from './visit-page.service';

import 'rxjs/add/operator/do';

@Injectable()
export class VisitItemResolver implements Resolve<any> {
  constructor(private service: VisitsService, private visitPage: VisitPageService) {
  }

  public resolve(route: ActivatedRouteSnapshot): Observable<any> {
    console.log(route.params.id);
    return this.service.getVisit(route.params.id);
      // .do((res) => this.visitPage.addCurrentStep(res));
  }
}

